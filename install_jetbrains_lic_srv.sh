#!/bin/bash
echo -e "\n[ PREREQUISITES ] Testing for prerequisites.. "
if ! command -v zip &> /dev/null
then echo
        "Program ZIP could not be found!"
	read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
	sudo apt-get -y install zip
else
	echo -e "[ OK ] Program ZIP is here!"
fi

if ! command -v unzip &> /dev/null
then echo
        "Program UNZIP could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
        sudo apt-get -y install unzip
else
        echo -e "[ OK ] Program UNZIP is here!"
fi

if ! command -v wget &> /dev/null
then echo
        "Program WGET could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
        sudo apt-get -y install wget
else
        echo -e "[ OK ] Program WGET is here!"
fi

if ! command -v java &> /dev/null
then echo
        "Java could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

	if [ ! -d /var/fls-jetbrains/ ]
	then
	        sudo mkdir /var/fls-jetbrains
		echo -e "[ CREATED ] Directory /var/fls-jetbrains/ created!"
	else
		echo -e "[ OK ] Directory /var/fls-jetbrains/ exist!"
	fi

	if [ ! -d /var/fls-jetbrains/src/ ]
        then
                sudo mkdir /var/fls-jetbrains/src
		echo -e "[ CREATED ] Directory /var/fls-jetbrains/src/ created!"
        else
                echo -e "[ OK ] Directory /var/fls-jetbrains/src/ exist!"
        fi

	cd /var/fls-jetbrains/src/
	# Download java 8.0 for FLS
	sudo wget https://www.michaldudek.it/download/fls-jetbrains/jdk-11.0.13_linux-x64_bin.tar.gz

	if [ ! -d /usr/lib/jvm/ ]
	then
		sudo mkdir /usr/lib/jvm
		echo "[ CREATED ] Directory /usr/lib/jvm created!"
	else
		echo -e "[ OK ] Directory /usr/lib/jvm/ exist!"
	fi

	sudo cp /var/fls-jetbrains/src/jdk-11.0.13_linux-x64_bin.tar.gz /usr/lib/jvm/
	cd /usr/lib/jvm/
	sudo tar zxvf /usr/lib/jvm/jdk-11.0.13_linux-x64_bin.tar.gz
	sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk-11.0.13/bin/java 4
	sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk-11.0.13/bin/javac 4
	sudo sudo update-alternatives --config java

else
	echo -e "[ OK ] Java is here!"

fi

echo -e "\n"
java -version

echo -e "\n[ INSTALL ] Install JetBrains Floating Licence server.."

if [ ! -d /var/fls-jetbrains/ ]
then
	sudo mkdir /var/fls-jetbrains
	echo -e "[ CREATED ] Directory /var/fls-jetbrains/ created!"
else
	echo -e "[ OK ] Directory /var/fls-jetbrains/ exist!"
fi

if [ ! -d /var/fls-jetbrains/src/ ]
then
        sudo mkdir /var/fls-jetbrains/src
	echo -e "[ CREATED ] Directory /var/fls-jetbrains/src/ created!"
else
	echo -e "[ OK ] Directory /var/fls-jetbrains/src/ exist!"
fi


cd /var/fls-jetbrains/src/
sudo wget https://download.jetbrains.com/lcsrv/license-server-installer.zip
sudo unzip license-server-installer.zip -d /var/fls-jetbrains/
cd /var/fls-jetbrains/

echo -e "[ DONE ] JetBrains Licence server is installed."
read -p "Do you want to change default PORT and host ADDRESS? (y/n)" changes

if [ "$changes" == "y" ] || [ "$changes" == "Y" ]
then
	echo -e "[ CHECKING ] Checking for running instances.."
	sudo /var/fls-jetbrains/bin/license-server.sh stop
	read -p "Set your new address: " newaddress
	read -p "Set your new port: " newport
	sudo /var/fls-jetbrains/bin/license-server.sh configure --listen $newaddress --port $newport
	echo -e "Configuration updated!"
fi

read -p "Do you want to start Licence server?? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || ex$
echo -e "[ CHECKING ] Checking for running instances.. "
sudo /var/fls-jetbrains/bin/license-server.sh stop
sudo /var/fls-jetbrains/bin/license-server.sh start

